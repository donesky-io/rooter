// Copyright 2018 Donesky, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package rooter

type Error403 struct {
	Message *string
}

func (e Error403) Error() string {
	if e.Message == nil {
		return "Access Denied"
	} else {
		return *e.Message
	}
}

type Error401 struct {
}

func (Error401) Error() string {
	return "Not Logged In"
}

type Error404 struct {
}

func (Error404) Error() string {
	return "Not Found"
}

type Error400 struct {
	Message string
}

func (e Error400) Error() string {
	return e.Message
}

type Error405 struct {
	Message string
}

func (e Error405) Error() string {
	return e.Message
}

