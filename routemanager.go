// Copyright 2018 Donesky, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package rooter

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"time"

	_ "expvar"

	"fmt"
)

type RouteManager interface {
	AddStaticHandler(path string, dir string)
	SetErrorHandlers(defaultHandler func(error interface{}, ctx RequestContext), notFoundHandler http.Handler)
	AddInterceptor(interceptor Interceptor)
	Path(pathUrl string) Path
	HandleFunc(path string, handler func(ctx RequestContext)) *mux.Route
	ServeHTTP(w http.ResponseWriter, r *http.Request)
	ListenAndServeExpVar(ipAddr string, port int)
	ListenAndServe(ipAddr string, port int)
}

type routeManager struct {
	router          RouteManagerRouter
	errorHandler    func(r interface{}, api RequestContext)
	notFoundHandler http.Handler
	paths           map[string]Path
	interceptors    []Interceptor
	readTimeout     time.Duration
	writeTimeout    time.Duration
}

func NewRouteManager(readTimeout, writeTimeout time.Duration) RouteManager {
	manager := &routeManager{
		router:       defaultRouter,
		paths:        make(map[string]Path),
		interceptors: make([]Interceptor, 0),
		readTimeout:  readTimeout,
		writeTimeout: writeTimeout,
	}
	manager.SetErrorHandlers(DefaultErrorHandler, &DefaultNotFoundHandler{
		ErrorHandler: DefaultErrorHandler,
	})
	return manager
}

func (m *routeManager) AddStaticHandler(path string, dir string) {
	m.router.PathPrefix(path).Handler(http.StripPrefix(path, http.FileServer(http.Dir(dir))))
}

func (m *routeManager) SetErrorHandlers(defaultHandler func(error interface{}, ctx RequestContext), notFoundHandler http.Handler) {
	m.errorHandler = defaultHandler
	m.notFoundHandler = notFoundHandler
	m.router.SetNotFoundHandler(m.notFoundHandler)
}

func (m *routeManager) AddInterceptor(interceptor Interceptor) {
	m.interceptors = append(m.interceptors, interceptor)
}

func (m *routeManager) Path(pathUrl string) Path {
	var p *path
	p, ok := m.paths[pathUrl].(*path)
	if ok && p != nil {
		return p
	}
	p = &path{
		handlers:     make(map[string]func(RequestContext)),
		interceptors: make([]Interceptor, 0),
	}
	p.route = m.HandleFunc(pathUrl, func(ctx RequestContext) {
		p.Execute(ctx.Method(), ctx)
	})
	m.paths[pathUrl] = p
	return p
}

func (m *routeManager) HandleFunc(path string, handler func(ctx RequestContext)) *mux.Route {
	muxHandler := func(w http.ResponseWriter, r *http.Request) {
		api := NewContext(w, r)
		defer func() {
			if r := recover(); r != nil {
				m.errorHandler(r, api)
			}
		}()
		for _, interceptor := range m.interceptors {
			if !interceptor.Intercept(api) {
				return
			}
		}
		handler(api)
	}
	return m.router.HandleFunc(path, muxHandler)
}

func (m *routeManager) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	m.router.ServeHTTP(w, r)
}

func (m *routeManager) ListenAndServeExpVar(ipAddr string, port int) {
	log.Printf("Listening at %s:%d", ipAddr, port)
	go http.ListenAndServe(fmt.Sprintf("%s:%d", ipAddr, port), nil)
}

func (m *routeManager) ListenAndServe(ipAddr string, port int) {
	log.Printf("Listening at %s:%d", ipAddr, port)
	srv := &http.Server{
		Handler:      m,
		Addr:         fmt.Sprintf("%s:%d", ipAddr, port),
		ReadTimeout:  m.readTimeout,
		WriteTimeout: m.writeTimeout,
	}
	log.Fatal(srv.ListenAndServe())
}
