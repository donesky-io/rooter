// Copyright 2018 Donesky, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package rooter

import (
	"github.com/gorilla/mux"
	"net/http"
)

type RouteManagerRouter interface {
	PathPrefix(path string) *mux.Route
	HandleFunc(path string, handler func(w http.ResponseWriter, r *http.Request)) *mux.Route
	SetNotFoundHandler(http.Handler)
	ServeHTTP(w http.ResponseWriter, r *http.Request)
}

type defaultRouteManagerRouter struct {
	router *mux.Router
}

var defaultRouter RouteManagerRouter = &defaultRouteManagerRouter{
	router: mux.NewRouter(),
}

func SetDefaultRouteManagerRouter(router RouteManagerRouter) {
	defaultRouter = router
}

func (r *defaultRouteManagerRouter) PathPrefix(path string) *mux.Route {
	return r.router.PathPrefix(path)
}

func (r *defaultRouteManagerRouter) HandleFunc(path string, handler func(w http.ResponseWriter, r *http.Request)) *mux.Route {
	return r.router.HandleFunc(path, handler)
}

func (r *defaultRouteManagerRouter) SetNotFoundHandler(handler http.Handler) {
	r.router.NotFoundHandler = handler
}

func (r *defaultRouteManagerRouter) ServeHTTP(w http.ResponseWriter, request *http.Request) {
	r.router.ServeHTTP(w, request)
}
