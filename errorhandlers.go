// Copyright 2018 Donesky, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package rooter

import (
	"log"
	"net/http"
	"runtime/debug"
)

type DefaultNotFoundHandler struct {
	ErrorHandler func(r interface{}, ctx RequestContext)
}

func (h *DefaultNotFoundHandler) ServeHTTP(w http.ResponseWriter, request *http.Request) {
	ctx := NewContext(w, request)
	h.ErrorHandler(Error404{}, ctx)
}

type Error struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func DefaultErrorHandler(r interface{}, ctx RequestContext) {
	var code int
	if _, ok := r.(Error401); ok {
		ctx.WriteHeader(401)
		code = 401
	} else if _, ok := r.(Error403); ok {
		ctx.WriteHeader(403)
		code = 403
	} else if _, ok := r.(Error400); ok {
		ctx.WriteHeader(400)
		code = 400
	} else if _, ok := r.(Error404); ok {
		ctx.WriteHeader(404)
		code = 404
	} else if _, ok := r.(Error405); ok {
		ctx.WriteHeader(405)
		code = 405
	} else {
		log.Printf("SERVER ERROR: %s: %s", r, debug.Stack())
		ctx.WriteHeader(500)
		code = 500
	}
	ctx.Serialize(&Error{
		Code:    code,
		Message: r.(error).Error(),
	})
}
