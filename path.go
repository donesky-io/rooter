// Copyright 2018 Donesky, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package rooter

import "github.com/gorilla/mux"

type Path interface {
	AddInterceptor(Interceptor) Path
	Get(func(RequestContext)) Path
	Put(func(RequestContext)) Path
	Post(func(RequestContext)) Path
	Delete(func(RequestContext)) Path
	Head(func(RequestContext)) Path
	Options(func(RequestContext)) Path
	All(func(RequestContext)) Path
	Execute(method string, ctx RequestContext)
}

type path struct {
	route        *mux.Route
	handlers     map[string]func(RequestContext)
	interceptors []Interceptor
	allHandler   func(RequestContext)
}

func (p *path) AddInterceptor(interceptor Interceptor) Path {
	p.interceptors = append(p.interceptors, interceptor)
	return p
}

func (p *path) Get(handler func(RequestContext)) Path {
	p.handlers["GET"] = handler
	return p
}

func (p *path) Put(handler func(RequestContext)) Path {
	p.handlers["PUT"] = handler
	return p
}

func (p *path) Post(handler func(RequestContext)) Path {
	p.handlers["POST"] = handler
	return p
}

func (p *path) Delete(handler func(RequestContext)) Path {
	p.handlers["DELETE"] = handler
	return p
}

func (p *path) Head(handler func(RequestContext)) Path {
	p.handlers["HEAD"] = handler
	return p
}

func (p *path) Options(handler func(RequestContext)) Path {
	p.handlers["OPTIONS"] = handler
	return p
}

func (p *path) All(handler func(RequestContext)) Path {
	p.allHandler = handler
	return p
}

func (p *path) Execute(method string, ctx RequestContext) {
	var handler func(RequestContext)
	if p.allHandler != nil {
		handler = p.allHandler
	} else {
		handler = p.handlers[method]
	}
	if handler == nil {
		panic(Error405{})
	}
	for _, interceptor := range p.interceptors {
		if !interceptor.Intercept(ctx) {
			return
		}
	}
	handler(ctx)
}
