// Copyright 2018 Donesky, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package rooter

import (
	"net/http"
	"context"
	"github.com/gorilla/mux"
	"encoding/json"
	"io/ioutil"
	"errors"
)

type mdUserKey struct{}

type RequestContext interface {
	GetUser() User
	SetUser(user User)
	Request() *http.Request
	Context() context.Context
	SetContext(context context.Context)
	GetPathParameters() map[string]string

	GetPathString(name string) string
	GetPathStringPtr(name string) *string
	GetPathInt32(name string) int32
	GetPathInt64(name string) int64
	TryGetPathInt32(name string) (int32, error)
	TryGetPathInt64(name string) (int64, error)

	GetRequestString(name string) string
	GetRequestStringPtr(name string) *string
	GetRequestInt32(name string) int32
	GetRequestInt64(name string) int64
	GetRequestFloat32(name string) float32
	GetRequestFloat64(name string) float64
	TryGetRequestInt32(name string) (int32, error)
	TryGetRequestInt64(name string) (int64, error)
	TryGetRequestFloat32(key string) (float32, error)
	TryGetRequestFloat64(name string) (float64, error)
	SafeGetRequestInt32(name string) int32
	SafeGetRequestInt64(name string) int64
	SafeGetRequestFloat32(name string) float32
	SafeGetRequestFloat64(name string) float64

	GetData(key string) interface{}
	SetData(key string, data interface{})

	Method() string
	Header() http.Header
	Cookie(name string) (*http.Cookie, error)
	AddCookie(cookie *http.Cookie)
	Cookies() []*http.Cookie
	Writer() http.ResponseWriter
	WriteHeader(statusCode int)
	Write(content []byte)
	Serialize(i interface{})
	Deserialize(i interface{})
}

type requestContext struct {
	writer     http.ResponseWriter
	request    *http.Request
	statusCode int
	varsRead   bool
	vars       map[string]string
	data       map[string]interface{}
}

func NewContext(writer http.ResponseWriter, request *http.Request) RequestContext {
	return &requestContext{
		writer:     writer,
		request:    request,
		statusCode: -1,
	}
}

func (ctx *requestContext) GetData(key string) interface{} {
	if ctx.data == nil {
		return nil
	}
	return ctx.data[key]
}

func (ctx *requestContext) SetData(key string, data interface{}) {
	if ctx.data == nil {
		ctx.data = make(map[string]interface{})
	}
	ctx.data[key] = data
}

func (ctx *requestContext) GetUser() User {
	user, ok := ctx.request.Context().Value(mdUserKey{}).(User)
	if !ok {
		panic(Error401{})
	}
	return user
}

func (ctx *requestContext) SetUser(user User) {
	ctx.SetContext(context.WithValue(ctx.request.Context(), mdUserKey{}, user))
}

func (ctx *requestContext) Request() *http.Request {
	return ctx.request
}

func (ctx *requestContext) Context() context.Context {
	return ctx.request.Context()
}

func (ctx *requestContext) SetContext(context context.Context) {
	ctx.request = ctx.request.WithContext(context)
}

func (ctx *requestContext) GetPathParameters() map[string]string {
	if !ctx.varsRead {
		ctx.varsRead = true
		ctx.vars = mux.Vars(ctx.request)
	}
	return ctx.vars
}

func (ctx *requestContext) Method() string {
	return ctx.request.Method
}

func (ctx *requestContext) Header() http.Header {
	return ctx.writer.Header()
}

func (ctx *requestContext) Cookie(name string) (*http.Cookie, error) {
	return ctx.request.Cookie(name)
}

func (ctx *requestContext) AddCookie(cookie *http.Cookie) {
	ctx.request.AddCookie(cookie)
}

func (ctx *requestContext) Cookies() []*http.Cookie {
	return ctx.request.Cookies()
}

func (ctx *requestContext) Writer() http.ResponseWriter {
	return ctx.writer
}

func (ctx *requestContext) WriteHeader(statusCode int) {
	if ctx.statusCode != -1 {
		panic(errors.New("status code already set"))
	}
	ctx.statusCode = statusCode
	ctx.writer.WriteHeader(statusCode)
}

func (ctx *requestContext) Write(content []byte) {
	ctx.writer.Write(content)
}

func (ctx *requestContext) Serialize(i interface{}) {
	body, err := json.Marshal(i)
	if err != nil {
		panic(err)
	}
	if ctx.statusCode == -1 {
		ctx.statusCode = 200
		ctx.writer.WriteHeader(200)
	}
	ctx.writer.Write(body)
}

func (ctx *requestContext) Deserialize(i interface{}) {
	body, err := ioutil.ReadAll(ctx.request.Body)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(body, i)
	if err != nil {
		panic(err)
	}
}
