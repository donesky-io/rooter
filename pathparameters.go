// Copyright 2018 Donesky, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package rooter

import (
	"strconv"
	"errors"
)

func (ctx *requestContext) GetRequestString(name string) string {
	return ctx.request.FormValue(name)
}

func (ctx *requestContext) GetRequestStringPtr(name string) *string {
	v := ctx.request.FormValue(name)
	if v != "" {
		return &v
	}
	return nil
}

func (ctx *requestContext) GetPathString(name string) string {
	return ctx.GetPathParameters()[name]
}

func (ctx *requestContext) GetPathStringPtr(name string) *string {
	p := ctx.GetPathParameters()[name]
	if p != "" {
		return &p
	}
	return nil
}

func (ctx *requestContext) GetPathInt32(name string) int32 {
	i, err := ctx.TryGetPathInt32(name)
	if err != nil {
		panic(err)
	}
	return i
}

func (ctx *requestContext) GetPathInt64(name string) int64 {
	i, err := ctx.TryGetPathInt64(name)
	if err != nil {
		panic(err)
	}
	return i
}

func (ctx *requestContext) GetRequestInt32(name string) int32 {
	i, err := ctx.TryGetRequestInt32(name)
	if err != nil {
		panic(err)
	}
	return i
}

func (ctx *requestContext) GetRequestInt64(name string) int64 {
	i, err := ctx.TryGetRequestInt64(name)
	if err != nil {
		panic(err)
	}
	return i
}

func (ctx *requestContext) GetRequestFloat32(name string) float32 {
	f, err := ctx.TryGetRequestFloat32(name)
	if err != nil {
		panic(err)
	}
	return f
}

func (ctx *requestContext) GetRequestFloat64(name string) float64 {
	f, err := ctx.TryGetRequestFloat64(name)
	if err != nil {
		panic(err)
	}
	return f
}

func (ctx *requestContext) TryGetPathInt32(name string) (int32, error) {
	val := ctx.GetPathParameters()[name]
	if val != "" {
		if i, err := strconv.ParseInt(val, 10, 32); err != nil {
			return 0, err
		} else {
			return int32(i), nil
		}
	}
	return 0, errors.New("empty val")
}

func (ctx *requestContext) TryGetPathInt64(name string) (int64, error) {
	val := ctx.GetPathParameters()[name]
	if val != "" {
		if i, err := strconv.ParseInt(val, 10, 64); err != nil {
			return 0, err
		} else {
			return i, nil
		}
	}
	return 0, errors.New("empty val")
}

func (ctx *requestContext) TryGetRequestInt32(name string) (int32, error) {
	val := ctx.request.FormValue(name)
	if val != "" {
		if i, err := strconv.ParseInt(val, 10, 32); err != nil {
			return 0, err
		} else {
			return int32(i), nil
		}
	}
	return 0, errors.New("empty val")
}

func (ctx *requestContext) TryGetRequestInt64(name string) (int64, error) {
	val := ctx.request.FormValue(name)
	if val != "" {
		if i, err := strconv.ParseInt(val, 10, 64); err != nil {
			return 0, err
		} else {
			return i, nil
		}
	}
	return 0, errors.New("empty val")
}

func (ctx *requestContext) TryGetRequestFloat32(key string) (float32, error) {
	val := ctx.request.FormValue(key)
	if val != "" {
		if i, err := strconv.ParseFloat(val, 32); err != nil {
			return 0, err
		} else {
			return float32(i), nil
		}
	}
	return 0, errors.New("empty val")
}

func (ctx *requestContext) TryGetRequestFloat64(name string) (float64, error) {
	val := ctx.request.FormValue(name)
	if val != "" {
		if i, err := strconv.ParseFloat(val, 64); err != nil {
			return 0, err
		} else {
			return i, nil
		}
	}
	return 0, errors.New("empty val")
}

func (ctx *requestContext) SafeGetRequestInt32(name string) int32 {
	i, _ := ctx.TryGetRequestInt32(name)
	return i
}

func (ctx *requestContext) SafeGetRequestInt64(name string) int64 {
	i, _ := ctx.TryGetRequestInt64(name)
	return i
}

func (ctx *requestContext) SafeGetRequestFloat32(name string) float32 {
	i, _ := ctx.TryGetRequestFloat32(name)
	return i
}

func (ctx *requestContext) SafeGetRequestFloat64(name string) float64 {
	i, _ := ctx.TryGetRequestFloat64(name)
	return i
}
