// Copyright 2018 Donesky, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package rooter

import (
	"strings"
	"encoding/base64"
)

type BasicAuthInterceptor struct {
	Authenticator Authenticator
}

func (i *BasicAuthInterceptor) Intercept(ctx RequestContext) bool{
	var token string
	if authorization := ctx.Header().Get("Authorization"); authorization != "" {
		parts := strings.Split(authorization, " ")
		if len (parts) == 2 && parts[0] == "Basic" {
			token = parts[1]
		}
	}
	if token == "" {
		panic(Error401{})
	}
	str, err := base64.StdEncoding.DecodeString(token)
	if err != nil {
		panic(Error401{})
	}
	userPass := string(str)
	userPassVals := strings.Split(userPass, ":")
	if len(userPassVals) != 2 {
		panic(Error401{})
	}
	user := i.Authenticator.PasswordAuthenticate(ctx, userPassVals[0], userPassVals[1])
	if user == nil {
		panic(Error401{})
	}
	ctx.SetUser(user)
	return true
}

